<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \App\BITM\SEIP117343\Student;
use \App\BITM\SEIP117343\Message;
use \App\BITM\SEIP117343\Utility;

$students = new Student();
//Utility::dd($students);
$student = $students->edit($_GET['id']);
//Utility::dd($student);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Edit</title>

        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body><div class="container"><br>
            <form action="update.php" method="post" class="form-inline">
                <div class="form-group col-md-10">
                    <input type="hidden" class="form-control" 
                           name="id"
                           id="id"
                           value="<?php echo $student->id; ?>">
                    <label for="title">Student name:</label>
                    <input type="text" name="title" class="form-control" placeholder="Student name"
                           value="<?php echo $student->title; ?>"><br><br>
                    <label for="ftitle">Father name:</label>
                    <input type="text" name="ftitle" class="form-control" placeholder="Father name"
                           value="<?php echo $student->ftitle; ?>"><br><br>
                    <label for="mtitle">Mother name:</label>
                    <input type="text" name="mtitle" class="form-control" placeholder="Mother name"
                           value="<?php echo $student->mtitle; ?>"><br><br>
                    <label for="birth">DOB:</label>
                    <input type="date" name="birth"
                           value="<?php echo $student->birth; ?>"><br><br>
                    <label for="gender">Gender:</label>
                    <label><input type="radio" name="gender" id="male" value="Male" <?php if(preg_match('/Male/', $student->gender)){ echo "checked";} ?>> Male</label>
                    <label><input type="radio" name="gender" id="female" value="Female"<?php if(preg_match('/Female/', $student->gender)){ echo "checked";} ?>> Female</label>
                    <?php  $student->gender;?><br><br>
                    <label for="email">Email: </label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email Address"
                           value="<?php echo $student->email; ?>"><br><br>
                    <label for="mobile">Mobile No.:</label>
                    <input type="text" name="mobile" class="form-control" placeholder="Mobile number"
                           value="<?php echo $student->mobile; ?>"><br><br>
                    <label for="address">Address:</label>
                    <textarea name="address" class="form-control" rows="3">
                              <?php echo $student->address; ?></textarea><br><br>
                    <label for="division">Division:</label>
                    <select class="form-control" name="division">
                        <option name="division" value="Sylhet" <?php if(preg_match('/Sylhet/', $student->division)){ echo "selected";} ?>>Sylhet</option>
                        <option name="division" value="Dhaka" <?php if(preg_match('/Dhaka/', $student->division)){ echo "selected";} ?>>Dhaka</option>
                        <option name="division" value="Chittagong" <?php if(preg_match('/Chittagong/', $student->division)){ echo "selected";} ?>>Chittagong</option>
                        <option name="division" value="Khulna" <?php if(preg_match('/Khulna/', $student->division)){ echo "selected";} ?>>Khulna</option>
                        <option name="division" value="Rajshahi" <?php if(preg_match('/Rajshahi/', $student->division)){ echo "selected";} ?>>Rajshahi</option>
                        <option name="division" value="Rangpur" <?php if(preg_match('/Rangpur/', $student->division)){ echo "selected";} ?>>Rangpur</option>
                        <option name="division" value="Barisal" <?php if(preg_match('/Barisal/', $student->division)){ echo "selected";} ?>>Barisal</option>
                        <option name="division" value="Mymensingh" <?php if(preg_match('/Mymensingh/', $student->division)){ echo "selected";} ?>>Mymensingh</option>
                    </select><?php  $student->division;?><br><br>
                    
                    <label for="course">Courses applied for:</label><br>
                    <label><input type="radio" name="course" id="course" value="BBA" <?php if(preg_match('/BBA/', $student->course)){ echo "checked";} ?>> BBA</label><br>
                    <label><input type="radio" name="course" id="course" value="BSc" <?php if(preg_match('/BSc/', $student->course)){ echo "checked";} ?>> BSc</label><br>
                    <label><input type="radio" name="course" id="course" value="English" <?php if(preg_match('/English/', $student->course)){ echo "checked";} ?>> English</label><br>
                    <label><input type="radio" name="course" id="course" value="Economics" <?php if(preg_match('/Economics/', $student->course)){ echo "checked";} ?>> Economics</label>
                    <?php  $student->course;?><br><br><br>
                    <button type="submit" class="btn btn-primary">Update</button><br><br>
                </div>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>

