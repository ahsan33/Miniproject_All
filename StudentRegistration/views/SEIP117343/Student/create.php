<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Student Registration form</title>

        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body><div class="container"><br>
            <form action="store.php" method="post" class="form-inline">
                <div class="form-group col-md-10">
                    <label for="title">Student name:</label>
                    <input type="text" name="title" class="form-control" placeholder="Student name"><br><br>
                    <label for="ftitle">Father name:</label>
                    <input type="text" name="ftitle" class="form-control" placeholder="Father name"><br><br>
                    <label for="mtitle">Mother name:</label>
                    <input type="text" name="mtitle" class="form-control" placeholder="Mother name"><br><br>
                    <label for="birth">DOB:</label>
                    <input type="date" name="birth"><br><br>
                    <label for="gender">Gender:</label>
                    <label><input type="radio" name="gender" id="male" value="Male"> Male</label>
                    <label><input type="radio" name="gender" id="female" value="Female"> Female</label><br><br>
                    <label for="email">Email: </label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email Address"><br><br>
                    <label for="mobile">Mobile No.:</label>
                    <input type="text" name="mobile" class="form-control" placeholder="Mobile number"><br><br>
                    <label for="address">Address:</label>
                    <textarea name="address" class="form-control" rows="3"></textarea><br><br>
                    <label for="division">Division:</label>
                    <select class="form-control" name="division">
                        <option>Sylhet</option>
                        <option>Dhaka</option>
                        <option>Chittagong</option>
                        <option>Khulna</option>
                        <option>Rajshahi</option>
                        <option>Rangpur</option>
                        <option>Barisal</option>
                        <option>Mymensingh</option>
                    </select><br><br>
                    <label for="course">Courses applied for:</label><br>
                    <label><input type="radio" name="course" id="course" value="BBA"> BBA</label><br>
                    <label><input type="radio" name="course" id="course" value="BSC"> BSc</label><br>
                    <label><input type="radio" name="course" id="course" value="English"> English</label><br>
                    <label><input type="radio" name="course" id="course" value="Economics"> Economics</label><br><br>
                    <button type="submit" class="btn btn-primary">Submit</button><br><br>
                </div>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
