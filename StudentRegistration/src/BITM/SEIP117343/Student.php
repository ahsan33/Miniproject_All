<?php

namespace App\BITM\SEIP117343;

use App\BITM\SEIP117343\Utility;
use App\BITM\SEIP117343\Message;

class Student {

    public $id = "";
    public $title = "";
    public $ftitle = "";
    public $mtitle = "";
    public $birth = "";
    public $gender = "";
    public $email = "";
    public $mobile = "";
    public $address = "";
    public $division = "";
    public $course = "";

    public function __construct() {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("registration") or die("Cannot select database.");
    }

    public function index() {
        //Utility::dd($_REQUEST);
        $student = array();
        
        $query = "SELECT * FROM `student`";
        
        $result = mysql_query($query);
        //Utility::dd($result);
        while ($row = mysql_fetch_assoc($result)) {
            $student[] = $row;
            //Utility::dd($row);
        }
        return $student;
    }

    public function store() {
        //echo 'world';
        $title = $_REQUEST['title'];
        $ftitle = $_REQUEST['ftitle'];
        $mtitle = $_REQUEST['mtitle'];
        $birth = $_REQUEST['birth'];
        $gender = $_REQUEST['gender'];
        $email = $_REQUEST['email'];
        $mobile = $_REQUEST['mobile'];
        $address = $_REQUEST['address'];
        $division = $_REQUEST['division'];
        $course = $_REQUEST['course'];
        $query = "INSERT INTO `registration`.`student` (`title`, `ftitle`, `mtitle`, `birth`, `gender`, `email`, `mobile`, `address`, `division`, `course`) VALUES ('{$this->title}', '{$this->ftitle}', '{$this->mtitle}', '{$this->birth}', '{$this->gender}', '{$this->email}', '{$this->mobile}', '{$this->address}', '{$this->division}', '{$this->course}');";
        
        //Utility::dd($query);
        if (mysql_query($query)) {
            Message::set('Student data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "SELECT * FROM `student` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

        public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "SELECT * FROM `student` WHERE id=" . $id;
        $b = mysql_query($query);
        //Utility::dd($result);
        $result = mysql_fetch_object($b);
        //Utility::dd($result);
        return $result;
    }

    
    public function update(){
//        $title = $_REQUEST['title'];
//        $ftitle = $_REQUEST['ftitle'];
//        $mtitle = $_REQUEST['mtitle'];
//        $birth = $_REQUEST['birth'];
//        $gender = $_REQUEST['gender'];
//        $email = $_REQUEST['email'];
//        $mobile = $_REQUEST['mobile'];
//        $address = $_REQUEST['address'];
//        $division = $_REQUEST['division'];
//        $course = $_REQUEST['course'];
        $query = "UPDATE `student` SET `title` = '".$this->title."', `ftitle` = '".$this->ftitle."', `mtitle` = '".$this->mtitle."', `birth` = '".$this->birth."', `gender` = '".$this->gender."', `email` = '".$this->email."', `mobile` = '".$this->mobile."',`address` = '".$this->address."', `division` = '".$this->division."', `course` = '".$this->course."' WHERE `student`.`id` = ".$this->id;

        if (mysql_query($query)) {
            Message::set('Student data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }
    
        public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "DELETE FROM `registration`.`student` WHERE `student`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Student data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }
    
    

    public function prepare($data = array()) {
       //Utility::dd($data);
        if (is_array($data) && array_key_exists('title', $data)) {

            $this->title = $data['title'];
            $this->ftitle = $data['ftitle'];
            $this->mtitle = $data['mtitle'];
            $this->birth = $data['birth'];
            $this->gender = $data['gender'];
            $this->email = $data['email'];
            $this->mobile = $data['mobile'];
            $this->address = $data['address'];
            $this->division = $data['division'];
            $this->course = $data['course'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
