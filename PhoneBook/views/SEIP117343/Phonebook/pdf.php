<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \Ahsan\BITM\SEIP117343\Phonebook;
use \Ahsan\BITM\SEIP117343\Message;

$phonebook = new Phonebook();
$phone = $phonebook->index();
$trs="";
?>


    <?php
    $sl = 0;
    foreach ($phone as $phonebook):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>" .$sl."</td>";
    $trs.="<td>" .$phonebook['title']."</td>";
    $trs.="<td>" .$phonebook['address']."</td>";
    $trs.="<td>" .$phonebook['hphone']."</td>";
    $trs.="<td>" .$phonebook['mphone']."</td>";
    $trs.="</tr>";
    endforeach;
?>

<?php
$html = <<<AHSAN
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>Student Registration</title>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
                <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
              </head>

                    <body>
                      
                        <div class="container">
                            <table class="table table-bordered" class="table table-responsive" style="width:100%">
                                <br>  <thead>
                                        <tr>
                                            <td><b>Sl.</b></td>
                                            <td><b>Name</b></td>		
                                            <td><b>Address</b></td>
                                            <td><b>Home phone</b></td>
                                            <td><b>Cell phone</b></td>
                                         </tr>
                                    </thead>
                                    <tbody>
                                    echo $trs;
                                    </tbody>
                                    </table>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                                        <!-- Include all compiled plugins (below), or include individual files as needed -->
                                        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
                                        
                                        </div>
                                        </body>
                                        </html>

AHSAN;
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "PhoneBook" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "mpdf" . DIRECTORY_SEPARATOR . "mpdf" . DIRECTORY_SEPARATOR . 'mpdf.php';
$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;



