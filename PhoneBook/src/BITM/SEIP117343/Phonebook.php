<?php

namespace Ahsan\BITM\SEIP117343;
use \Ahsan\BITM\SEIP117343\Utility;
use \Ahsan\BITM\SEIP117343\Message;

class Phonebook {

    public $id = "";
    public $title = "";
    public $address = "";
    public $hphone = "";
    public $mphone = "";

    public function __construct() {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
    }

        public function index() {
        //Utility::dd($_REQUEST);
        $phonebook = array();
        
        $query = "SELECT * FROM `phone`";
        
        $result = mysql_query($query);
        //Utility::dd($result);
        while ($row = mysql_fetch_assoc($result)) {
            $phonebook[] = $row;
            //Utility::dd($row);
        }
        return $phonebook;
    }
    public function store() {
        $title = $_REQUEST['title'];
        $address = $_REQUEST['address'];
        $hphone = $_REQUEST['hphone'];
        $mphone = $_REQUEST['mphone'];

        $query = "INSERT INTO `phonebook`.`phone` (`title`, `address`, `hphone`, `mphone`) VALUES ('{$this->title}', '{$this->address}', '{$this->hphone}', '{$this->mphone}');";
        //Utility::dd($query);
        if (mysql_query($query)) {
            Message::set('Phone data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }
    
        public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "SELECT * FROM `phone` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }
    
     public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "SELECT * FROM `phone` WHERE id=" . $id;
        $b = mysql_query($query);
        //Utility::dd($result);
        $result = mysql_fetch_object($b);
        //Utility::dd($result);
        return $result;
    }

    
    public function update(){

        $query = "UPDATE `phone` SET `title` = '".$this->title."', `address` = '".$this->address."', `hphone` = '".$this->hphone."', `mphone` = '".$this->mphone."' WHERE `phone`.`id` = ".$this->id;

        if (mysql_query($query)) {
            Message::set('Phonebook data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }
    
        public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $query = "DELETE FROM `phonebook`.`phone` WHERE `phone`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Phonebook data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }

    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('title', $data)) {

            $this->title = $data['title'];
            $this->address = $data['address'];
            $this->hphone = $data['hphone'];
            $this->mphone = $data['mphone'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
